/**
 * Created by whill on 6/29/15.
 */
print("Evaling n1.js");

var
	f1 = function(name) {
		print('In anon func, called via f1 FCO ref ' + name);

		return "returning from f1 FCO ref";
	},
	f2 = function(object) {
		print("START f2 function\n{");

		print("LINE 16 - The \"Type\" of the object passed from the Java class: " + Object.prototype.toString.call(object));
		print("LINE 17 - Object internals: " + object.toString());
		print("LINE 18 - Start modifying map as JSON...");
		print("LINE 19 - object get key named \"first name\": " + object['first name']);
		print("LINE 20 - change value for key named \"first name\" to 'Winfield'");
		object['first name'] = 'Winfield';
		print("LINE 22 - End modifying map as JSON");
		print("LINE 23 - return the JSONObject (on line 25) to calling Java class");
		print("END f2 function\n}");

		return object;
	};