import com.google.gson.Gson;
import org.json.simple.JSONObject;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.io.FileReader;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.*;

/**
 * Created by whill on 6/29/15.
 * ...
 */

public class main {

    private static final Logger LOGGER = Logger.getLogger(LogNash.class.getName());

    public static void main(String[] args) {
        Long st = (new Date()).getTime();

        Handler consoleHandler = null;
        Handler fileHandler = null;

        Gson gson = new Gson();
        try {
            consoleHandler = new ConsoleHandler();
            fileHandler = new FileHandler("./logs/nashornErrors.log");

            LOGGER.addHandler(consoleHandler);
            LOGGER.addHandler(fileHandler);
            consoleHandler.setLevel(Level.ALL);
            fileHandler.setLevel(Level.ALL);

            LOGGER.setLevel(Level.ALL);
            LOGGER.config("Config done.");

            //LOGGER.removeHandler(consoleHandler);

            LOGGER.log(Level.ALL,"Start time: " + st);

            ScriptEngineManager factory = new ScriptEngineManager();

            ScriptEngine engine = factory.getEngineByName("nashorn");
            ScriptEngine appleScriptEngine = factory.getEngineByName("appleScriptEngine");

            engine.eval(new FileReader("jvmJS/n1.js"));
            Invocable invocable = (Invocable)engine;
            //Object result = invocable.invokeFunction("f1", "Win Hill");
            //System.out.println(result);
            //System.out.println(result.getClass());

            invocable.invokeFunction("f2", new Date());
            Map jsMapTest = new HashMap<String, String>();
            jsMapTest.put("first name", "Win");
            jsMapTest.put("last name", "Hill");
            jsMapTest.put("age", "35");
            Object f2Result = invocable.invokeFunction("f2", new org.json.simple.JSONObject(jsMapTest));

            JSONObject jso = (JSONObject)f2Result;
            System.out.println("Map.get the 'last name' key value - LINE 62: " + jso.get("last name"));

            System.out.println("Confirm the 'first name' key value was changed to \"Winfield\" - LINE 64: " + f2Result);

            /*
             * Type type = new TypeToken<Map<String, String>>(){}.getType();
             * Map<String, String> myMap = gson.fromJson(f2Result.toString(), type);
             * System.out.println(myMap.toString());
             * System.out.println(myMap.get("last name"));
             */

            engine.eval(
                "(function(){" +
                    "print('----');" +
                    "var ts = java.lang.System.currentTimeMillis();" +
                    "print('Time Start: ' + ts);" +
                    "var file = new java.io.File('logs/nashornErrors.log');" +
                    "print(file.getAbsolutePath());" +
                    "print(file.getName());" +
                    "print('File size = ' + file.getTotalSpace());" +
                    "for(var i = 0; i < 5; i++) {" +
                        "print('Hi!');" +
                    "}" +
                    "var te = java.lang.System.currentTimeMillis();" +
                    "print('Time End: ' + te);" +
                    "print('Diff in milliseconds: ' + (te-ts));" +
                    "print('Diff in seconds: ' + (te-ts) / 1000);" +
                    "print('----');" +
                "}());"
            );

            Long et = (new Date()).getTime();

            LOGGER.log(Level.ALL, "End time: " + et);
            LOGGER.log(Level.ALL, "Total Time in milliseconds: " + (et-st));
            LOGGER.log(Level.ALL, "Total Time in seconds: " + ((float)(et-st) / 1000));
        } catch(Exception exception) {
            LOGGER.log(Level.SEVERE, exception.toString(), exception);
        }
    }
}
